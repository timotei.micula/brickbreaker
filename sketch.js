var lungPaleta=200,paletaY=550;
var eX=240,eY=520,eW=20,eH=20;
var directionX=-1,directionY=-1,pas=0;
var canvasHeight=600,canvasWidth=600;
var nrBricks=5;
var bricks1= [], bricks2=[],bricks3=[];
var brickWidth=(canvasWidth - 30 * 2 - 10 * (nrBricks - 1)) /nrBricks;
var brickHeight=20;

for(var i=0;i<nrBricks;i++){
  bricks1.push(1);
  bricks2.push(1);
  bricks3.push(1);
}
function setup() {
  createCanvas(canvasHeight, canvasWidth);
}
function draw() {
  background(0);
  // cheat();
  paleta();
  ball();
  wallBounce();
  bricksShow();
  brickBreaker();
  doubleClicked();
  win();
}
function mouseClicked(event){
  if(pas==5)
  pas=0;
  else
  pas=5;
  
}
function doubleClicked(event){
  if(event){
    reset();
    pas=0;
  lungPaleta=200;
  }
}
function reset(){
  for(var i=0;i<nrBricks;i++){
    bricks1[i]=1;
    bricks2[i]=1;
    bricks3[i]=1;
  }
  eX=240;
  eY=520;
  directionX=-1;
  directionY=-1;
  pas=5;
}
function win(){
  var win=1;
  for(var i=0;i<nrBricks;i++)
    {if(bricks1[i]==1 || bricks2[i]==1 || bricks3[i]==1)
      win=0;
    }
  if(win==1){
  console.log("Ai castigat");
  if(lungPaleta>50)
  lungPaleta-=50;
  else
  lungPaleta=10;
  reset();
  pas=0;  
  
}
}
function cheat(){
  rect(eX-lungPaleta/2,550,lungPaleta,10);
  if(eY+eW/2==paletaY){
    var pB=eX-lungPaleta/2; //paleta begin
    var pE=eX+lungPaleta/2; //paleta end
    if(eX>=pB && eX<=pE)
      {directionY=-directionY;
      }
    }
}
function paleta(){
  rect(mouseX-lungPaleta/2, paletaY, lungPaleta, 10);
  paletaBounce();
}
function ball(){
  if(pas==0 && eY==520){
    eX=mouseX;
  }
  ellipse(eX,eY,eW,eH);
}
function  wallBounce(){
  if(eX-eW/2<0 || eX+eW/2>canvasWidth)
    directionX=-directionX;
  if(eY-eH/2<0 )
    directionY=-directionY;
  if(eY+eH/2>=canvasHeight)
    pas=0;
  eY=eY+directionY*pas;
  eX=eX+directionX*pas;
}
function paletaBounce(){
  if(eY+eH/2==paletaY){
  var pB=mouseX-lungPaleta/2; //paleta begin
  var pE=mouseX+lungPaleta/2; //paleta end
  if(eX>=pB && eX<=pE)
    {directionY=-directionY;
    }
  }
}
function bricksShow(){
  for(var i=0;i<nrBricks;i++) //row 1
    if(bricks1[i]==1)
      rect(30 + i * (brickWidth+10), 40 , brickWidth, 20);
  for(var i=0;i<nrBricks;i++){  //row 2
    if(bricks2[i]==1)
      rect(30+i*(brickWidth+10),40*2 ,brickWidth,brickHeight);
   }
   for(var i=0;i<nrBricks;i++){  //row 3
    if(bricks3[i]==1)
      rect(30+i*(brickWidth+10),40*3 ,brickWidth,brickHeight);
   }
}
function brickBreaker(){
  // console.log(eY);
  if(eY-eH/2==40*3+brickHeight)   //row 3 bottom side
    for(var i=0;i<nrBricks;i++)
    {var beginB=30+i* (brickWidth+10);
      if(eX>=beginB && eX<=beginB+brickWidth && bricks3[i]!=0 &&directionY==-1)
        {bricks3[i]=0;
        directionY=-directionY;
        console.log("JOS");
      }
      
  }
  if(eY-eH/2==40*2+brickHeight)   //row 2 bottom side
    for(var i=0;i<nrBricks;i++)
    {var beginB=30+i* (brickWidth+10);
      if(eX>=beginB && eX<=beginB+brickWidth && bricks2[i]!=0 &&directionY==-1)
        {bricks2[i]=0;
        directionY=-directionY;
        console.log("JOS");
      }
     
  }
  if(eY-eH/2==40*1+brickHeight)   //row 1 bottom side
    for(var i=0;i<nrBricks;i++)
    {var beginB=30+i* (brickWidth+10);
      if(eX>=beginB && eX<=beginB+brickWidth && bricks1[i]!=0 &&directionY==-1)
        {bricks1[i]=0;
        directionY=-directionY;
        console.log("JOS");
      }
      
  }
  //top side
  if(eY+eH/2==40*3)   //row 3 top side
    for(var i=0;i<nrBricks;i++)
    {var beginB=30+i* (brickWidth+10);
      if(eX>=beginB && eX<=beginB+brickWidth && bricks3[i]!=0  &&directionY==1)
        {bricks3[i]=0;
        directionY=-directionY;
        console.log("SUS");
      }
      
  }
  if(eY+eH/2==40*2)   //row 2 top side
    for(var i=0;i<nrBricks;i++)
    {var beginB=30+i* (brickWidth+10);
      if(eX>=beginB && eX<=beginB+brickWidth && bricks2[i]!=0  &&directionY==1)
        {bricks2[i]=0;
        directionY=-directionY;
        console.log("SUS");
      }
     
  }
  if(eY+eH/2==40*1)   //row 1 top side
    for(var i=0;i<nrBricks;i++)
    {var beginB=30+i* (brickWidth+10);
      if(eX>=beginB && eX<=beginB+brickWidth && bricks1[i]!=0  &&directionY==1)
        {bricks1[i]=0;
        directionY=-directionY;
        console.log("SUS");
      }
      
  }
  //right side

  for(var i=0;i<nrBricks;i++){  //row 3
    beginB=30+i* (brickWidth+10);
    if(eX-eH/2<=beginB+brickWidth && eX>= beginB+ brickWidth){
      if(eY+eH/2>=40*3 && eY+eH/2<=40*3+brickHeight && bricks3[i]!=0 && directionX==-1)
        {
          bricks3[i]=0;
          directionX=-directionX;
          console.log("DREAPTA");
          
        }
        
    }
  }
  for(var i=0;i<nrBricks;i++){  //row 2
    beginB=30+i* (brickWidth+10);
    if(eX-eH/2<=beginB+brickWidth && eX>= beginB+ brickWidth){
      if(eY+eH/2>=40*2 && eY+eH/2<=40*2+brickHeight && bricks2[i]!=0 && directionX==-1)
        {
            bricks2[i]=0;
            directionX=-directionX;
            console.log("DREAPTA");
          }
      }
    }
  for(var i=0;i<nrBricks;i++){  //row 1
    beginB=30+i* (brickWidth+10);
    if(eX-eH/2<=beginB+brickWidth && eX>= beginB+ brickWidth){
       if(eY+eH/2>=40*1 && eY+eH/2<=40*1+brickHeight && bricks1[i]!=0 && directionX==-1)
         {
          bricks1[i]=0;
          directionX=-directionX;
          console.log("DREAPTA");
        }
      }
      
  }
  //left side
  for(var i=0;i<nrBricks;i++){  //row 3
    beginB=30+i* (brickWidth+10);
    if(eX+eH/2<=beginB && eX>= beginB){
       if(eY+eH/2>40*3 && eY+eH/2<40*3+brickHeight && bricks3[i]!=0 && directionX==1)
         {
          bricks3[i]=0;
          directionX=-directionX;
          console.log("STANGA");
        }
      }
  }
  for(var i=0;i<nrBricks;i++){  //row 2
    beginB=30+i* (brickWidth+10);
    if(eX+eH/2<=beginB && eX>= beginB){
       if(eY+eH/2>=40*2 && eY+eH/2<=40*2+brickHeight && bricks2[i]!=0 && directionX==1)
         {
          bricks2[i]=0;
          directionX=-directionX;
          console.log("STANGA");
        }
      }
  }
  for(var i=0;i<nrBricks;i++){  //row 1
    beginB=30+i* (brickWidth+10);
    if(eX+eH/2<=beginB && eX>= beginB){
       if(eY+eH/2>=40*1 && eY+eH/2<=40*1+brickHeight && bricks1[i]!=0 && directionX==1)
         {
          bricks1[i]=0;
          directionX=-directionX;
          console.log("STANGA");
        }
      }
  }
}
function cerc(x,y){
    ellipse(x,y,15,15);
    ellipse(x,y,15,15);
}
